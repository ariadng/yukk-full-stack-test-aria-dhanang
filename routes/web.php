<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppController;
use App\Http\Controllers\WebController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\TransactionController;

// Unauthenticated routes
Route::middleware(["guest"])->group(function () {
	// User registration & authentication
	Route::get("/",					[WebController::class, "index"]);
	Route::get("/login",			[UserController::class, "showLogin"])->name("login");
	Route::post("/handle-login",	[UserController::class, "handleLogin"]);
	Route::get("/register",			[UserController::class, "showRegister"]);
	Route::post("/handle-register",	[UserController::class, "handleRegister"]);
});

// Authenticated routes
Route::middleware(["auth"])->group(function () {
	// App
	Route::prefix("app")->group(function () {
		Route::get("/", 						[AppController::class, "showHome"])->middleware("auth")->name("app");
		Route::get("/top-up",					[TransactionController::class, "showTopUp"]);
		Route::get("/transaction",				[TransactionController::class, "showTransaction"]);
		Route::get("/view/{transaction}",		[TransactionController::class, "viewTransaction"]);
		Route::get("/edit/{transaction}",		[TransactionController::class, "editTransaction"]);
		Route::get("/delete/{transaction}",		[TransactionController::class, "deleteTransaction"]);
		Route::post("/add-transaction", 		[TransactionController::class, "create"]);
		Route::post("/update-transaction/{id}", [TransactionController::class, "update"]);
		Route::get("/history",					[HistoryController::class, "showHistory"]);
	});
	// Logout
	Route::get("/logout",			[UserController::class, "handleLogout"]);
});

