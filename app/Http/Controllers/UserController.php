<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller {
	
	// Display user login page
	public function showLogin() {
		return view("auth.login");
	} // ↑ showLogin

	// Handle user login
	public function handleLogin(Request $request) {

		// Validate user input
		$input = $request->validate([
			"email"		=> ["required"],
			"password"	=> ["required"],
		]);

		// Log user in
		if (auth()->attempt($input)) {
			$request->session()->regenerate();
			return redirect("/app");
		}

		// Login failed
		return redirect("login")->with([
			"error" => "Email atau password salah.",
			"email" => $input["email"]
		]);

	} // ↑ handleLogin
	
	// Display user registration page
	public function showRegister() {
		return view("auth.register");
	} // ↑ showRegister

	// Handle user registration
	public function handleRegister(Request $request) {

		// Validate user input
		$input = $request->validate([
			"name"		=> ["required", "max:100"],
			"email"		=> ["required", "email", "max:100", Rule::unique("users", "email")],
			"password"	=> ["required", "max:100"],
		]);

		// Hash user password
		$input["password"] = bcrypt($input["password"]);

		// Create a new user
		$user = User::create($input);

		// Log the newly created user in
		auth()->login($user);
		return redirect("/");

	} // ↑ handleRegister

	// Log user out
	public function handleLogout() {
		auth()->logout();
		return redirect("/");
	} // ↑ handleLogout
	
} // ↑ UserController
