<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\File;

class TransactionController extends Controller {

	// Show top up form
	public function showTopUp() {
		return view("app.topup");
	}

	// Show transaction form
	public function showTransaction() {
		return view("app.transaction");
	}

	// View a transaction
	public function viewTransaction(Transaction $transaction) {
		return view("app.transaction-view", [
			"transaction" => $transaction
		]);
	}

	// Edit a transaction
	public function editTransaction(Transaction $transaction) {
		return view("app.transaction-edit", [
			"transaction" => $transaction
		]);
	}

	// Delete a transaction
	public function deleteTransaction(Transaction $transaction) {
		$transaction->delete();
		return redirect("/app");
	}
	
	// Create new transaction
	public function create(Request $request) {
		
		// Validate user input
		$input = $request->validate([
			"type"		=> ["required"],
			"amount"	=> ["numeric", "required", "max:".($request->get("type") === "top_up" ? "100000000" : auth()->user()->balance()), "min:1"],
			"date"		=> ["required"],
			"notes"		=> ["required", "max:256"],
			"receipt"	=> ["image", File::image()->max('5mb')],
		]);

		$input["type"]		= strip_tags($input["type"]);
		$input["amount"]	= strip_tags($input["amount"]);
		$input["date"]		= strip_tags($input["date"]);
		$input["notes"]		= strip_tags($input["notes"]);
		$input["receipt"]	= array_key_exists("receipt", $input) ? strip_tags($input["receipt"]) : null;
		$input["user_id"] 	= auth()->id();

		$debit 	= $input["type"] == "top_up" ? $input["amount"] : 0;
		$credit = $input["type"] == "top_up" ? 0 : $input["amount"];
		$identifier = Str::random(14);

		$receipt = null;
		if ($request->has("receipt")) {
			$receipt = $request->file("receipt")->store("receipts", "public");
		}

		// Create new transaction
		Transaction::create([
			"type"			=> $input["type"],
			"identifier"	=> $identifier,
			"date"			=> $input["date"],
			"notes"			=> $input["notes"],
			"receipt"		=> $receipt,
			"user_id"		=> $input["user_id"],
			"debit"			=> $debit,
			"credit"		=> $credit,
		]);

		return redirect("/app");

	}

	// Update a transaction
	public function update(string $id, Request $request) {
		// Validate user input
		$input = $request->validate([
			"amount"	=> ["numeric", "required", "max:".($request->get("type") === "top_up" ? "100000000" : auth()->user()->balance()), "min:1"],
			"date"		=> ["required"],
			"notes"		=> ["required", "max:256"]
		]);

		$data = Transaction::find($id);

		$input["amount"]	= strip_tags($input["amount"]);
		$input["date"]		= strip_tags($input["date"]);
		$input["notes"]		= strip_tags($input["notes"]);

		$debit 	= $data->type == "top_up" ? $input["amount"] : 0;
		$credit = $data->type == "top_up" ? 0 : $input["amount"];

		$data->debit = $debit;
		$data->credit = $credit;
		$data->date = $input["date"];
		$data->notes = $input["notes"];
		$data->save();

		return redirect("/app");

	}

}
