<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller {
	
	// Display app home page
	public function showHome() {
		// Current user's latest transactions
		$transactions = auth()->user()->transactions()
			->orderBy("date", "desc")
			->orderBy("created_at", "desc")
			->limit(5)->get();

		// Render the view
		return view("app.home", [
			"transactions" => $transactions
		]);
	} // ↑ function showHome

} // ↑ class AppController
