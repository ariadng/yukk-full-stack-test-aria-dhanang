<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class HistoryController extends Controller {
    
    // Display the list of transaction history
    public function showHistory(Request $request) {

        $query = auth()->user()->transactions();

        // Type filter
        if ($request->query("type")) {
            $type = $request->query("type");
            if ($type != "all") $query = $query->where("type", "=", $type);
        }

        // Search text
        if ($request->query("search")) {
            $search = $request->query("search");
            $query = $query->where(function (Builder $query) {
                $query->orWhereRaw("LOWER(notes) LIKE LOWER(?)", ['%' . request()->query("search") . '%'])
                      ->orWhereRaw("LOWER(identifier) LIKE LOWER(?)", ['%' . request()->query("search") . '%']);
            });
        }

        // Order filter
        $sort = $request->query("sort");
        switch ($sort) {
            case "type":
                $query = $query->orderBy("type", "asc");
                break;

            case "amount_low":
                $query = $query
                    ->orderBy("credit", "desc")
                    ->orderBy("debit", "asc");
                break;

            case "amount_high":
                $query = $query
                    ->orderBy("debit", "desc")
                    ->orderBy("credit", "asc");
                break;
            
            default:
                $query = $query
                    ->orderBy("date", "desc")
                    ->orderBy("created_at", "desc");
                break;
        }

        // Pagination
        $transactions = $query->paginate(5)->withQueryString();

        return view("app.history", [
            "transactions" => $transactions,
            "search" => $request->query("search"),
            "type" => $request->query("type"),
            "sort" => $request->query("sort")
        ]);
    }

}
