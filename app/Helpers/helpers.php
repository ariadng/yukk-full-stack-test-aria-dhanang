<?php
// app/Helpers/helpers.php

if (! function_exists('formatPrice')) {
    function format_price($number) {
        $sign = $number < 0 ? "-" : "";
        $formattedNumber = number_format(abs($number), 2, ',', '.');
        return $sign . "Rp " . $formattedNumber;
    }
}