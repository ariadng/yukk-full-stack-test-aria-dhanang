<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transaction extends Model
{
    use HasFactory;
    protected $fillable = ["type", "identifier", "date", "debit", "credit", "notes", "receipt", "user_id"];

    public function getFormattedType() {
        return $this->attributes["type"] == "top_up" ? "Top-up" : "Transaksi";
    }

    public function getFormattedDate() {
        $date = Carbon::parse($this->attributes["date"])->locale("id");
        return $date->isoFormat("dddd, D MMM YYYY");
    }

    public function getAmount() {
        return $this->attributes["type"] == "top_up" ? $this->attributes["debit"] : $this->attributes["credit"] * -1;
    }

    public function getAbsoluteAmount() {
        return abs($this->getAmount());
    }

    public function getFormattedAmount() {
        return format_price($this->getAmount());
    }

    public function getReceiptURL() {
        if ($this->attributes["receipt"]) {
            return asset('storage/' . $this->attributes["receipt"]);
        }
        return "";
    }

}
