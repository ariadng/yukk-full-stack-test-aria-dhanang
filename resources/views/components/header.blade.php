<div class="Header">
	<div class="Container">
		<div class="Brand">
			<a href="/">
				<img src="/images/yukk.png" />
			</a>
		</div>

		<div class="Menu">
			@auth

			@else
				<a class="MenuItem {{ (request()->is('login')) ? 'Active' : '' }}" href="/login">Login</a>
				<a class="MenuItem {{ (request()->is('register')) ? 'Active' : '' }}" href="/register">Daftar</a>
			@endauth
		</div>
		
		@auth
		<div class="User">
			<span class="Text">
				<span class="Name">{{ auth()->user()->name }}</span>
				<div class="Email">{{ auth()->user()->email }}</div>
			</span>
		</div>
		@endauth

		<div class="Actions">
			@auth
				<a class="Button LogoutButton" href="/logout">
					<span class="Label">Logout</span>
				</a>
			@else

			@endauth
		</div>
	</div>
</div>