@extends("layouts.default")

@section("content")
	<div class="Container TransactionPage">
		<div class="TransactionForm">

			<header>
				<a href="/app" class="Button">
					<span class="Icon">arrow_back</span>
					<span class="Label">Kembali</span>
				</a>
			</header>

			@if ($errors->any())
				<div class="Errors">
					<div class="Title">Pendaftaran gagal</div>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			<div class="Tabs">
				<a class="Tab {{ (request()->is('app/top-up')) ? 'Active' : '' }}" href="/app/top-up">Top-up</a>
				<a class="Tab {{ (request()->is('app/transaction')) ? 'Active' : '' }}" href="/app/transaction">Transaksi</a>
			</div>

			<form class="Form" enctype="multipart/form-data" action="/app/add-transaction" method="POST">
				@csrf

				<input type="hidden" name="type" value="top_up">

				<div class="FormField">
					<input type="number" name="amount" id="amount" min="1" max="100000000" value="{{ old('amount') }}" required />
					<label for="amount">Nominal (Rp)</label>
				</div>

				<div class="FormField">
					<input type="date" name="date" id="date" value="{{ old('date') }}" required />
					<label for="date">Tanggal</label>
				</div>

				<div class="FormField">
					<textarea name="notes" id="notes" rows="2" maxlength="256" required>{{ old('notes') }}</textarea>
					<label for="notes">Keterangan</label>
				</div>

				<div class="FormField">
					<input type="file" name="receipt" id="receipt" accept="image/*" value="{{ old('receipt') }}" required />
					<label for="receipt">Bukti Top-up</label>
				</div>

				<div class="Actions">
					<button class="Button Primary" type="submit">Simpan</button>
				</div>

			</form>

		</div>
	</div>
@endsection