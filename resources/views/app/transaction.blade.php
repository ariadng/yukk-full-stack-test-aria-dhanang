@extends("layouts.default")

@section("content")
	<div class="Container TransactionPage">
		<div class="TransactionForm">

			<header>
				<a href="javascript:history.back()" class="Button">
					<span class="Icon">arrow_back</span>
					<span class="Label">Kembali</span>
				</a>
				<div class="Balance">
					<span class="Label">Saldo saat ini</span>
					<span class="Value">{{ auth()->user()->formattedBalance() }}</span>
				</div>
			</header>

			@if ($errors->any())
				<div class="Errors">
					<div class="Title">Input gagal</div>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			<div class="Tabs">
				<a class="Tab {{ (request()->is('app/top-up')) ? 'Active' : '' }}" href="/app/top-up">Top-up</a>
				<a class="Tab {{ (request()->is('app/transaction')) ? 'Active' : '' }}" href="/app/transaction">Transaksi</a>
			</div>

			@if (auth()->user()->balance() > 0)

			<form class="Form" action="/app/add-transaction" method="POST">
				@csrf

				<input type="hidden" name="type" value="transaction">

				<div class="FormField">
					<input type="number" name="amount" min="1" max="{{ auth()->user()->balance() }}" value="{{ old('amount') }}" required />
					<label for="amount">Nominal (Rp)</label>
				</div>

				<div class="FormField">
					<input type="date" name="date" value="{{ old('date') }}" required />
					<label for="date">Tanggal</label>
				</div>

				<div class="FormField">
					<textarea name="notes" id="notes" rows="2" maxlength="256" required>{{ old('notes') }}</textarea>
					<label for="notes">Keterangan</label>
				</div>

				<div class="Actions">
					<button class="Button Primary" type="submit">Simpan</button>
				</div>

			</form>

			@else
			<div class="EmptyBalance">
				Anda tidak memiliki saldo. Silakan top-up terlebih dahulu.
			</div>
			@endif

		</div>
	</div>
@endsection