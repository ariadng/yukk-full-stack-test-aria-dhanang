@extends("layouts.default")

@section("content")
	<div class="Container HistoryPage">

		<form class="Form" method="GET" action="/app/history">

			<header>
				<a href="/" class="Button">
					<span class="Icon">arrow_back</span>
					<span class="Label">Kembali</span>
				</a>
				<h3>Riwayat Transaksi</h3>
				<div class="Balance">
					<span class="Label">Saldo saat ini</span>
					<span class="Value">{{ auth()->user()->formattedBalance() }}</span>
				</div>
				<div class="SearchForm">
					<div class="FormField Horizontal">
						<input type="text" name="search" placeholder="Cari transaksi..." value="{{$search}}" />
						<button class="Button Primary">
							<div class="Icon">search</div>
						</button>
					</div>
				</div>
			</header>

			<div class="Filter">
				<section>
					<div class="Title">Jenis transaksi</div>
					<div class="Filters">
						<div class="Selector">
							<label>
								<input type="radio" name="type" value="all" {{ ($type != "top_up" && $type != "transaction") ? "checked" : "" }} />
								<span class="Label">Semua</span>
							</label>
							<label>
								<input type="radio" name="type" value="top_up" {{ $type == "top_up" ? "checked" : "" }} />
								<span class="Label">Top-up</span>
							</label>
							<label>
								<input type="radio" name="type" value="transaction" {{ $type == "transaction" ? "checked" : "" }} />
								<span class="Label">Transaksi</span>
							</label>
						</div>
					</div>
				</section>
				<section>
					<div class="Title">Urutkan</div>
					<div class="Filters">
						<div class="Dropdown">
							<select name="sort">
								<option value="date" {{ $sort == "date" ? "selected" : "" }}>Tanggal</option>
								<option value="type" {{ $sort == "type" ? "selected" : "" }}>Jenis transaksi</option>
								<option value="amount_low" {{ $sort == "amount_low" ? "selected" : "" }}>Nominal (rendah &rarr; tinggi)</option>
								<option value="amount_high" {{ $sort == "amount_high" ? "selected" : "" }}>Nominal (tinggi &rarr; rendah)</option>
							</select>
							<span class="Icon">expand_more</span>
						</div>
					</div>
				</section>
				<div class="FilterActions">
					<a href="/app/history" class="Button Reset">
						<span class="Icon">refresh</span>
					</a>
					<button class="Button Primary" type="submit">Terapkan</button>
				</div>
			</div>

		</form>
		
		@if (sizeof($transactions) > 0)
		<div class="List">
			<div class="ListHeader">
				<div class="Column">Ref</div>
				<div class="Column">Jenis Transaksi</div>
				<div class="Column">Tanggal</div>
				<div class="Column">Keterangan</div>
				<div class="Column Right">Jumlah</div>
			</div>
			@foreach ($transactions as $transaction)
			<a class="ListItem" href="/app/view/{{ $transaction->id }}">
				<span class="Column">{{$transaction->identifier}}</span>
				<span class="Column">{{$transaction->getFormattedType()}}</span>
				<span class="Column">{{$transaction->getFormattedDate()}}</span>
				<span class="Column">{{$transaction->notes}}</span>
				<span class="Column Amount {{ $transaction->type == 'top_up' ? 'Positive' : 'Negative' }}">
					{{$transaction->getFormattedAmount()}}
				</span>
			</a>
			@endforeach
		</div>

		<div class="Pagination">
			{{ $transactions->links() }}
		</div>

		@else
		<div class="EmptyTransactions">
			Belum ada transaksi.
		</div>
		@endif

	</div>
@endsection