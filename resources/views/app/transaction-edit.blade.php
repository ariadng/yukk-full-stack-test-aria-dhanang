@extends("layouts.default")

@section("content")
	<div class="Container TransactionPage">
		<div class="TransactionForm">

			<header>
				<a href="javascript:history.back()" class="Button">
					<span class="Icon">arrow_back</span>
					<span class="Label">Kembali</span>
				</a>
				<div class="Balance">
					<span class="Label">Saldo saat ini</span>
					<span class="Value">{{ auth()->user()->formattedBalance() }}</span>
				</div>
			</header>

			@if ($errors->any())
				<div class="Errors">
					<div class="Title">Input gagal</div>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			<div class="Type">
				@if ($transaction->type == "top_up")
				<span class="Icon">account_balance_wallet</span>
				<div class="Text">
					<span class="Title">Top-up</span>
					<span class="Ref">{{ $transaction->identifier }}</span>
				</div>
				@else
				<span class="Icon">shopping_cart</span>
				<div class="Text">
					<span class="Title">Transaksi</span>
					<span class="Ref">{{ $transaction->identifier }}</span>
				</div>
				@endif
			</div>

			@if (auth()->user()->balance() > 0)

			<form class="Form" action="/app/update-transaction/{{ $transaction->id }}" method="POST">
				@csrf

				<input type="hidden" name="type" value="{{ $transaction->type }}">

				<div class="FormField">
					<input type="number" name="amount" min="1" max="{{ $transaction->type == "top_up" ? "100000000" : auth()->user()->balance() }}" value="{{ $transaction->getAbsoluteAmount() }}" required />
					<label for="amount">Nominal (Rp)</label>
				</div>

				<div class="FormField">
					<input type="date" name="date" value="{{ $transaction->date }}" required />
					<label for="date">Tanggal</label>
				</div>

				<div class="FormField">
					<textarea name="notes" id="notes" rows="2" maxlength="256" required>{{ $transaction->notes }}</textarea>
					<label for="notes">Keterangan</label>
				</div>

				<div class="Actions">
					<button class="Button Primary" type="submit">Simpan</button>
				</div>

			</form>

			@else
			<div class="EmptyBalance">
				Anda tidak memiliki saldo. Silakan top-up terlebih dahulu.
			</div>
			@endif

		</div>
	</div>
@endsection