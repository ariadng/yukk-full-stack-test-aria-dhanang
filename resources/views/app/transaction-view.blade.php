@extends("layouts.default")

@section("content")
	<div class="Container TransactionPage">
		<div class="TransactionForm">

			<header>
				<a href="javascript:history.back()" class="Button">
					<span class="Icon">arrow_back</span>
					<span class="Label">Kembali</span>
				</a>
				<div class="Actions">
					<a href="/app/edit/{{ $transaction->id }}" class="Button">
						<span class="Icon">edit</span>
						<span class="Label">Edit</span>
					</a>
					<a href="/app/delete/{{ $transaction->id }}" class="Button">
						<span class="Icon">delete</span>
						<span class="Label">Hapus</span>
					</a>
				</div>
			</header>

			<div class="Type">
				@if ($transaction->type == "top_up")
				<span class="Icon">account_balance_wallet</span>
				<div class="Text">
					<span class="Title">Top-up</span>
					<span class="Ref">{{ $transaction->identifier }}</span>
				</div>
				@else
				<span class="Icon">shopping_cart</span>
				<div class="Text">
					<span class="Title">Transaksi</span>
					<span class="Ref">{{ $transaction->identifier }}</span>
				</div>
				@endif
			</div>

			<div class="Form" enctype="multipart/form-data" action="/app/add-transaction" method="POST">
				@csrf

				<input type="hidden" name="type" value="top_up">

				<div class="FormField">
					<div class="Amount">{{ $transaction->getFormattedAmount() }}</div>
					<label for="amount">Nominal</label>
				</div>

				<div class="FormField">
					<div class="Date">{{ $transaction->getFormattedDate() }}</div>
					<label for="date">Tanggal</label>
				</div>

				<div class="FormField">
					<div class="Notes">{{ $transaction->notes }}</div>
					<label for="notes">Keterangan</label>
				</div>

				@if ($transaction->type == "top_up")
				<div class="FormField">
					@if ($transaction->receipt)
					<img src="{{ $transaction->getReceiptURL() }}" />
					@else
					-
					@endif
					<label for="receipt">Bukti Top-up</label>
				</div>
				@endif

			</div>

		</div>
	</div>
@endsection