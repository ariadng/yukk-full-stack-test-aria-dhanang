@extends("layouts.default")

@section("content")
	<div class="Container App_Home">
		
		<div class="HUD">

			<div class="Balance">
				<span class="Label">Saldo</span>
				<span class="Amount">{{auth()->user()->formattedBalance()}}</span>
			</div>

			<div class="Menu">
				<a class="MenuItem" href="/app/top-up">
					<span class="Icon">account_balance_wallet</span>
					<span class="Label">Top Up</span>
				</a>
				<a class="MenuItem" href="/app/transaction">
					<span class="Icon">add</span>
					<span class="Label">Transaksi</span>
				</a>
				<a class="MenuItem" href="/app/history">
					<span class="Icon">history</span>
					<span class="Label">Riwayat</span>
				</a>
			</div>

		</div>

		@if (sizeof($transactions) > 0)
		<div class="LatestTransactions">
			<h3>Transaksi terbaru</h3>
			<div class="List">
				<div class="ListHeader">
					<div class="Column">Ref</div>
					<div class="Column">Jenis Transaksi</div>
					<div class="Column">Tanggal</div>
					<div class="Column">Keterangan</div>
					<div class="Column">Jumlah</div>
				</div>
				@foreach ($transactions as $transaction)
				<a class="ListItem" href="/app/view/{{ $transaction->id }}">
					<span class="Column">{{$transaction->identifier}}</span>
					<span class="Column">{{$transaction->getFormattedType()}}</span>
					<span class="Column">{{$transaction->getFormattedDate()}}</span>
					<span class="Column">{{$transaction->notes}}</span>
					<span class="Column Amount {{ $transaction->type == 'top_up' ? 'Positive' : 'Negative' }}">
						{{$transaction->getFormattedAmount()}}
					</span>
				</a>
				@endforeach
			</div>
			<div class="Actions">
				<a href="/app/history" class="Button Primary">
					<span class="Label">Lihat selengkapnya</span>
					<span class="Icon">arrow_forward</span>
				</a>
			</div>
		</div>
		@else
		<div class="EmptyTransactions">
			Belum ada transaksi.
		</div>
		@endif

	</div>
@endsection