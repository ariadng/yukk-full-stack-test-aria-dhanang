@extends("layouts.default")

@section("content")
	<div class="Container RegistrationPage">
		<div class="RegistrationForm">

			<h2>Daftar</h2>
			<p class="Lead">
				Silakan isi form berikut ini.
			</p>

			@if ($errors->any())
				<div class="Errors">
					<div class="Title">Pendaftaran gagal</div>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			<form class="Form" action="/handle-register" method="POST">
				@csrf
				
				<div class="FormField">
					<input type="text" id="name" name="name" maxlength="100" value="{{ old('name') }}" required autofocus />
					<label for="name">Nama Lengkap</label>
				</div>

				<div class="FormField">
					<input type="email" id="email" name="email" maxlength="100" value="{{ old('email') }}" required />
					<label for="email">Alamat Email</label>
				</div>

				<div class="FormField">
					<input type="password" id="password" name="password" maxlength="100" required />
					<label for="password">Password</label>
				</div>

				<div class="Actions">
					<button class="Button Primary" type="submit">
						Submit
					</button>
				</div>

			</form>
		</div>
	</div>
@endsection