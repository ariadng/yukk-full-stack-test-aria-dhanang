@extends("layouts.default")

@section("content")
	<div class="Container LoginPage">
		<div class="LoginForm">

			<h2>Login</h2>
			<p class="Lead">
				Silakan login terlebih dahulu.
			</p>

			@if (session("error"))
			<div class="Error">
				{{ session("error") }}
			</div>
			@endif

			<form class="Form" action="/handle-login" method="POST">
				@csrf

				<div class="FormField">
					<input type="email" id="email" name="email" maxlength="100" value="{{ session('email') }}" required autofocus />
					<label for="email">Alamat Email</label>
				</div>

				<div class="FormField">
					<input type="password" id="password" name="password" maxlength="100" required />
					<label for="password">Password</label>
				</div>

				<div class="Actions">
					<button class="Button Primary" type="submit">
						Login
					</button>
				</div>

			</form>
		</div>
	</div>
@endsection