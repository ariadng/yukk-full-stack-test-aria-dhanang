<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("identifier")->unique();
            $table->string("type");
            $table->date("date");
            $table->longText("notes");
            $table->unsignedBigInteger("debit")->default(0);
            $table->unsignedBigInteger("credit")->default(0);
            $table->string("receipt")->nullable();
            $table->foreignId("user_id")->constrained();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
